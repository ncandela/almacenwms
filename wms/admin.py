from django.contrib import admin
from .models import Almacen,Estructura,Unidad,Presentacion,PresentacionxUbicacion,TipoEstructura,EstructuraAlmacen,EntradaCabecera,\
    EntradaDetalle,Hotel,PedidoCabecera,PedidoDetalle,PreciosMedios,Configuracion,TipoImpuesto,TipoMovimiento,UnidadesxPresentacion, \
    Grupo
#MovimientoCab,MovimientoLin
# Register your models here.

def cambiarpicking(modeladmin, request, queryset):

    for ubicacion in queryset:
        ubicacion.picking = not ubicacion.picking
        ubicacion.save()

cambiarpicking.short_description = 'Cambiar Picking'

class DetalleInline(admin.TabularInline):
    model = EntradaDetalle
    extra = 0


class EstructuraAlmacenInline(admin.TabularInline):
    model = EstructuraAlmacen

class AlmaceAdmin(admin.ModelAdmin):
    list_display = ('nombre','direccion')
    inlines = [EstructuraAlmacenInline,]


class TipoEstructuraAdmin(admin.ModelAdmin):
    list_display = ('tipo','codificacion','estructurapadre','almacenaje','zonasalida')

admin.site.register(Almacen,AlmaceAdmin)

class EstructuraAdmin(admin.ModelAdmin):
    list_display = ('almacen','tipo','ubicacion','picking','tipo','get_almacenaje')
    search_fields = ('ubicacion',)
    list_filter = ('almacen','tipo','orden')
    actions = [cambiarpicking,]

    def get_almacenaje(self, obj):
        if obj.tipo.almacenaje:
            return 'Si'
        else:
            return 'No'

    get_almacenaje.short_description = 'Almacenaje'
    get_almacenaje.admin_order_field = 'tipo__almacenaje'


class CabeceraAdmin(admin.ModelAdmin):
    list_display = ('transaccion','fecha','tipo','proveedor','documento','almaorigen','almadest','moneda','importe','subtotal','impuestos','total')
    search_fields = ('transaccion','documento')
    inlines = [DetalleInline,]


class UndxPresentInline(admin.TabularInline):
    model = UnidadesxPresentacion
    fields = ('presentacion','unidad','unidades','unidadreferencia','uso')
    extra = 0

class PresentacionAdmin(admin.ModelAdmin):
    list_display = ('codigo','codarticulo','descripcion','grupo','unidad')
    search_fields = ('descripcion','codigo','codarticulo',)
    inlines = [UndxPresentInline,]



class PedidoDetalleInline(admin.TabularInline):
    model = PedidoDetalle
    extra = 0
class PedidosCabeceraAdmin(admin.ModelAdmin):
    list_display = ('fecha','hotel','documento')
    inlines = [PedidoDetalleInline,]


class PresentacionxUbicacionAdmin(admin.ModelAdmin):
    list_display = ('ubicacion','presentacion','unidad','stock','unidades','costo','fecha')
    search_fields = ['ubicacion',]



admin.site.register(PedidoCabecera,PedidosCabeceraAdmin)
admin.site.register(Estructura,EstructuraAdmin)
admin.site.register(Unidad)
admin.site.register(Presentacion,PresentacionAdmin)
admin.site.register(PresentacionxUbicacion,PresentacionxUbicacionAdmin)
admin.site.register(TipoEstructura,TipoEstructuraAdmin)
admin.site.register(EntradaCabecera,CabeceraAdmin)
admin.site.register(Hotel)
admin.site.register(Configuracion)
admin.site.register(TipoImpuesto)
admin.site.register(TipoMovimiento)
admin.site.register(Grupo)
