from django.shortcuts import render,redirect,HttpResponseRedirect,HttpResponse
from django.urls import reverse_lazy
from .models import Almacen,Estructura,TipoEstructura,EstructuraAlmacen,EntradaCabecera,EntradaDetalle,Presentacion,Unidad,PresentacionxUbicacion,PreciosMedios,Configuracion
from .models import PedidoCabecera,PedidoDetalle
import cx_Oracle
import itertools,string
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView,CreateView,DetailView,UpdateView
from .forms import PedidoForm,PedidoDetalleFormSet,PedidoDetalleForm,UnidadxPresentacionForm #,MovimientoLinForm,MovimientoLinFormSet
from .models import TipoMovimiento,UnidadesxPresentacion
from django.db import transaction

from .forms import UnidadxPresentacionForm

import math
import json

def convierte_und(unidad):

    unidad = unidad.upper()

    if unidad in ('PIEZS',
                  'PIEAZA',
                  'PIEZAS.',
                  'PIEIZA',
                  'PIEZAS',
                  'PIUEZAS',
                  'PIZA',
                  'PIZAS',
                  'PIEZ<AS',
                  'PIPEZA',
                  'PIEZAAS',
                  'PIEAS',
                  'PIEZAS1',
                  'PIIEZA',
                  'PIEA',
                  'PIEZSA',
                  'PIPEZAS',
                  'PIUEZA',
                  'PZA', 'PZAS', ' PIEZAS', 'POEZA', 'PUEZA', 'UNIDAD', 'PEZAS', 'PEIZA', '1'):
        unidad = 'PIEZA'

        if unidad in ('KILOS', 'KLO', ' KILOS', 'KILOA', 'KILOGRAMOS'):
            unidad = 'KILO'

        if unidad in ('MTS', 'METROS', ' METROS', 'MATRO','TRAMO'):
            unidad = 'METRO'

        if unidad in ( 'ROLLOS'):
            unidad = 'ROLLO'

        if unidad in ('PAQUETE', 'CAJAS'):
            unidad = 'CAJA'

        if unidad in ('LOTRO', 'LITROS'):
            unidad = 'LITRO'

        if unidad in ('GARRAFON','GARRAFAS'):
            unidad = 'GARRAFA'

        if unidad in ('CUEBTA', 'CUBTE', 'CUBEYA', 'CUBTE'):
            unidad = 'CUEBETA'

        if unidad in ('BOLSAS', 'BULTO'):
            unidad = 'BOLSA'

        if unidad in ('BOTES','LATAS'):
            unidad = 'LATA'

    return unidad




def addunidad(request,pk):
    template = 'almacen/addunidades.html'
    if request.method == 'GET':
        form = UnidadxPresentacionForm(request.GET or None)

    elif request.method == 'POST':
        form = UnidadxPresentacionForm(request.POST)

        if form.is_valid():
            movcab = form.save()
            pimporte = 0
            ptotal = 0



class SeriesIterator:
    def __init__(self,tipo='C',caracteres=1):
        if tipo == 'C':
            self.lista = list(map(''.join, itertools.product(string.ascii_uppercase, repeat=caracteres)))

        if tipo == 'c':
            self.lista = list(map(''.join, itertools.product(string.ascii_lowercase, repeat=caracteres)))

        if tipo == 'D':
            self.lista = list(map(''.join, itertools.product(string.digits, repeat=caracteres)))

        if tipo == 'H':
            self.lista = list(map(''.join, itertools.product(string.hexdigits, repeat=caracteres)))

        self.indice = 0

    def __iter__(self):

        self.lista[self.indice]

        return self

    def __next__(self):
        self.indice +=1
        if self.indice > len(self.lista):
            raise StopIteration

        return self.lista[self.indice]


# Create your views here.
def creaestructura(idAlmacen):

    almacen = Almacen.objects.get(pk=idAlmacen)

    if almacen.bloquearconfiguracionestructura == False:

        Estructura.objects.filter(almacen=almacen).delete()

        #conf1 = almacen.estructuraalmacen_set.filter(tipoestructura__estructurapadre = None).order_by('tipoestructura__orden')
        conf1 = EstructuraAlmacen.objects.filter(almacen=almacen, padre = None).order_by('tipoestructura__orden')
        #conf1 = EstructuraAlmacen.objects.filter(almacen=almacen).order_by('tipoestructura__orden')

        #Creamos las estructuras padres

        for e in conf1:

            if e.alfabetico:
                it = SeriesIterator('C',e.caracteres).lista
                contador = 0
                fin = e.cantidad
            if e.autonumerar:
                it = SeriesIterator('D',e.caracteres).lista
                contador = 1 #Para que no tenga en cuenta el 00....0
                fin = e.cantidad +1

            while contador < fin :

                estructura =Estructura(almacen=almacen, tipo=e.tipoestructura, ubicacion='{}'.format(it[contador]),padre=e.padre,orden=contador)
                estructura.save()
                contador += 1
                creahijos(estructura)


def creahijos(pEstructura):

    tipoestructura = TipoEstructura.objects.filter(estructurapadre=pEstructura.tipo).order_by('tipoestructura__orden')

    for tipo in tipoestructura:
        configuracion = EstructuraAlmacen.objects.filter(almacen=pEstructura.almacen,tipoestructura=tipo)
        for es in configuracion:

            if es.alfabetico:
                it = SeriesIterator('C', es.caracteres).lista
                contador = 0
                fin = es.cantidad
            if es.autonumerar:
                it = SeriesIterator('D', es.caracteres).lista
                contador = 1  # Para que no tenga en cuenta el 00....0
                fin = es.cantidad + 1


            while contador < fin :

                estructura = Estructura(almacen=pEstructura.almacen, tipo=es.tipoestructura,ubicacion='{}{}'.format(pEstructura.ubicacion,it[contador]),padre=pEstructura,orden=contador)
                contador += 1
                #if es.tipoestructura.almacenaje:
                estructura.save()
                creahijos(estructura)

def index(request):

    #Leer configuracion

    request.session['idpresentacion'] = ''
    request.session['idlinea'] = ''
    request.session['descripcion'] = ''

    alm = Almacen.objects.filter(pordefecto=True)
    if alm.exists():
        request.session['idalmacen'] = alm[0].pk
        request.session['almacen'] = alm[0].nombre
    else:
        return render(request, 'almacen/error.html', {'error': 'No hay almacen por defecto, por favor configure uno', 'transa': ''})


    return render(request,'almacen/index.html')


def carga(request):
    print('Carga')

    print(request.path)
    if 'entradas' in request.path:
        print('Entrada')
        return render(request,'almacen/recupera_transa.html',context={'tipo':'Entradas'})
    else:
        print('Salida')
        return render(request, 'almacen/recupera_transa.html',context={'tipo':'Salidas'})


# Trae el movimineto y la muestra
def recuperamov(request,idtransa,tipomov):
    print('Recupera Mov')
    #tipo 1 - entradas 0- salidas

    print('Tipomov:',tipomov)

    ok,error = importamovimiento(idtransa,tipomov)

    if ok:
        transaccion = EntradaCabecera.objects.get(transaccion=idtransa)
        if tipomov=='1':
            return render(request,'almacen/entrada.html',context={'transaccion':transaccion}) #,'ubicaciones':ubicaciones
        else:

            ## Asignacion de posiciones
            for linea in transaccion.entradadetalle_set.filter(ubicacion__isnull=True):

                pxu = PresentacionxUbicacion.objects.filter(almacen_id= request.session['idalmacen'],presentacion_id=linea.presentacion_id,ubicacion__estructura__picking=True).order_by('fecha').first()

                if pxu and pxu.ubicacion:
                    linea.ubicacion = pxu.ubicacion.ubicacion
                else:
                    linea.ubicacion = None
                linea.save()

            return render(request, 'almacen/entrada.html',context={'transaccion': transaccion})  # ,'ubicaciones':ubicaciones

    else:
        return render(request,'almacen/error.html',{'error':error,'transa':idtransa})


#Generamos el desglose de  una entrada y la llevamos a moviminetos.
def Desglosa(request,pk):
    pass

    # lineas = EntradaDetalle.objects.filter(transaccion_id=pk)
    # tmov = TipoMovimiento.objects.filter(tipomov=1).first()
    # movcab = MovimientoCab(tipo=tmov , fecha=datetime.date.today() ).save()
    # for linea in lineas:
    #     present = Presentacion.objects.get(codigo=linea.presentacion_id)
    #     und = present.unidadesxpresentacion_set.filter(unidad=linea.unidad).first()
    #
    #     numero_de_unidades = math.ceil(linea.cantidad/und.unidades)
    #
    #     restante = linea.cantidad
    #     for n in range(numero_de_unidades):
    #         cantidad,restante = divmod(restante/und.unidades)
    #         MovimientoLin(movimiento=movcab,presentacion=linea.presentacion,cantidad=1, stock=cantidad,unidad=linea.unidad).save()
    #
    # return render(request,'almacen/movimientodetail.html',context={'movimiento':movcab})



def importamovimiento(pTransaccion,tipo):
    # tipo 1 - entradas 0- salidas
    cfg = Configuracion.objects.first() #Traemos la configuracion
    # if cfg.unidadalmacenaje == None:
    #     return (False, 'Falta unidad de almacenaje en configuracion')

    if tipo == 1: #Entrada

        if EntradaCabecera.objects.filter(transaccion=pTransaccion).exists():
            return(True,'')

        # Conectamos con la base de datos
        try:
            con = cx_Oracle.connect('algoc15/service@192.168.1.25:1521/ZEUS')

        except Exception as e:
            print('Error: {}'.format(e))
            return(False,e)

        cur = con.cursor()

        # Generamos SQL
        sql = '''select ME_TRANSA,ME_TIPO,ME_PROVEEDOR,ME_DOCUMENTO,ME_FECHA,ME_ALMA,NVL(ME_ALMAREL,' '),ME_IMPORTE,ME_SUBTOTAL,ME_IVA,ME_TOTAL,NVL(ME_MONEDA,'MEX') from ALMOVENC WHERE ME_TRANSA = '{transa}' '''.format(transa=pTransaccion)

        try:
            cur.execute(sql)
            print('')
        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        try:
            cabrow = cur.fetchone() #Solo puede traer un resultado
            print(cabrow)
        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        if cabrow == None:
            return (False, 'Transacción no encontrada')


        #sql = ''' SELECT MD_ID,MD_PRESENTA,MD_CANTIDAD,MD_COSTO,MD_IMPORTE,MD_SUBTOTAL,MD_IVA,MD_TOTAL FROM ALMOVDET WHERE ME_TRANSA = '{transa}' '''
        sql = '''SELECT MD_ID,MD_PRESENTA,MD_CANTIDAD,MD_COSTO,MD_IMPORTE,MD_SUBTOTAL,MD_IVA,MD_TOTAL,PN_PRESENTA,PN_DESC,PN_UNIDAD_ENTRADA,PN_FACTOR_UMB FROM ALMOVDET LEFT JOIN ALPRESENTA
    ON MD_PRESENTA = PN_PRESENTA 
    WHERE Md_TRANSA = '{transa}' '''.format(transa=pTransaccion)
        try:
            cur = con.cursor()
            cur.execute(sql)

        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        try:
            detrows = cur.fetchall()

        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        # Si todo ha funcionado correctamente

        # Inserto en mis tablas
        #       0       1       2           3           4           5           6               7           8        9      10      11
        # ME_TRANSA,ME_TIPO,ME_PROVEEDOR,ME_DOCUMENTO,ME_FECHA,ME_ALMA,NVL(ME_ALMAREL,' '),ME_IMPORTE,ME_SUBTOTAL,ME_IVA,ME_TOTAL,ME_MONEDA

        cabecera = EntradaCabecera(transaccion=cabrow[0],tipo=cabrow[1],proveedor=cabrow[2],documento=cabrow[3],fecha=cabrow[4],almaorigen=cabrow[5],almadest=cabrow[6],importe=cabrow[7],
                            subtotal=cabrow[8],impuestos=cabrow[9],total=cabrow[10],moneda=cabrow[11])
        cabecera.save()

        for rows in detrows:
            #  0         1            2          3          4            5          6       7           8          9        10                11
            #MD_ID, MD_PRESENTA, MD_CANTIDAD, MD_COSTO, MD_IMPORTE, MD_SUBTOTAL, MD_IVA, MD_TOTAL, PN_PRESENTA, PN_DESC, PN_UNIDAD_ENTRADA, PN_FACTOR_UMB
            # Comento las unidades, se tienen que utilizar las que tiene el sistema
            # Suponemos que de cielo todo viene en unidades unicas
            # try:
            #     u,createdu = Unidad.objects.get_or_create(codigo= convierte_und(rows[10]),descripcion=rows[10])
            #
            # except Exception as e:
            #     cabecera.delete()
            #     print(e)
            #     cur.close()
            #     con.close()
            #     return (False, e)
            print(rows)
            u = cfg.unidadpordefecto

            try:
                #Si la presentacion no existe la creamos
                p = Presentacion.objects.get(codigo=rows[8])
                if p:
                    Presentacion(codigo=rows[8],descripcion=rows[9],unidad=u).save()

            except Exception as e:
                cabecera.delete()
                print(e)
                cur.close()
                con.close()
                return (False, e)

            try:

                und = p.unidadesxpresentacion_set.filter(uso='E')
                #Buscamos para la resentacion cual es la Unidad de entrada
                if und:
                    unidad = und[0].unidad

                else:
                    unidad = None

                lineas = EntradaDetalle(transaccion=cabecera,linea=rows[0],presentacion=p,cantidad=rows[2],costo=rows[3],importe=rows[4],subtotal=rows[5],impuestos=rows[6],total=rows[7],unidad= unidad,cantidadxasignar=rows[2])
                lineas.save()

            except Exception as e:
                cabecera.delete()
                print(e)
                cur.close()
                con.close()
                return (False, e)


        cur.close()
        con.close()
        return(True,'')

    else: #Salida
        if PedidoCabecera.objects.filter(transaccion=pTransaccion).exists():
            return (True, '')

        # Conectamos con la base de datos
        try:
            con = cx_Oracle.connect('algoc15/service@192.168.1.25:1521/ZEUS')

        except Exception as e:
            print('Error: {}'.format(e))
            return (False, e)

        cur = con.cursor()

        # Generamos SQL
        sql = '''select ME_TRANSA,ME_TIPO,ME_PROVEEDOR,ME_DOCUMENTO,ME_FECHA,ME_ALMA,NVL(ME_ALMAREL,' '),ME_IMPORTE,ME_SUBTOTAL,ME_IVA,ME_TOTAL,NVL(ME_MONEDA,'MEX') from ALMOVENC WHERE ME_TRANSA = '{transa}' '''.format(
            transa=pTransaccion)

        try:
            cur.execute(sql)
            print('')
        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        try:
            cabrow = cur.fetchone()  # Solo puede traer un resultado
            print(cabrow)
        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        if cabrow == None:
            return (False, 'Transacción no encontrada')

        # sql = ''' SELECT MD_ID,MD_PRESENTA,MD_CANTIDAD,MD_COSTO,MD_IMPORTE,MD_SUBTOTAL,MD_IVA,MD_TOTAL FROM ALMOVDET WHERE ME_TRANSA = '{transa}' '''
        sql = '''SELECT MD_ID,MD_PRESENTA,MD_CANTIDAD,MD_COSTO,MD_IMPORTE,MD_SUBTOTAL,MD_IVA,MD_TOTAL,PN_PRESENTA,PN_DESC,PN_UNIDAD_ENTRADA,PN_FACTOR_UMB FROM ALMOVDET LEFT JOIN ALPRESENTA
        ON MD_PRESENTA = PN_PRESENTA 
        WHERE Md_TRANSA = '{transa}' '''.format(transa=pTransaccion)
        try:
            cur = con.cursor()
            cur.execute(sql)

        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

        try:
            detrows = cur.fetchall()

        except Exception as e:
            print(e)
            cur.close()
            con.close()
            return (False, e)

            #       0       1       2           3           4           5           6               7           8        9      10      11
            # ME_TRANSA,ME_TIPO,ME_PROVEEDOR,ME_DOCUMENTO,ME_FECHA,ME_ALMA,NVL(ME_ALMAREL,' '),ME_IMPORTE,ME_SUBTOTAL,ME_IVA,ME_TOTAL,ME_MONEDA



            pedido = PedidoCabecera(transaccion=cabrow[0], documento=cabrow[3],
                                       fecha=cabrow[4],  almadest=cabrow[6], importe=cabrow[7],
                                       subtotal=cabrow[8], impuestos=cabrow[9], total=cabrow[10], moneda=cabrow[11])
            pedido.save()

            for rows in detrows:
                #  0         1            2          3          4            5          6       7           8          9        10                11
                # MD_ID, MD_PRESENTA, MD_CANTIDAD, MD_COSTO, MD_IMPORTE, MD_SUBTOTAL, MD_IVA, MD_TOTAL, PN_PRESENTA, PN_DESC, PN_UNIDAD_ENTRADA, PN_FACTOR_UMB
                try:

                    und_cielo = convierte_und(row[10])
                    p=Presentacion.objects.get(codigo=rows[8])
                    und = p.unidadesxpresentacion_set.filter(uso='S').first()
                    if und:
                        unidad = und
                    else:
                        unidad = p.unidadesxpresentacion_set.filter(uso='E').first()

                    if und_cielo != unidad.unidad:
                        pass
                        #error se estan pidiendo unidades que no existem
                    #Suponemos que de cielo siempre vienen los pedido en unidades
                    num_unidades =   rows[2]/unidad.unidades
                    #Buscamo Ubicacion. (solo picking)
                    pxu = PresentacionxUbicacion.objects.filter(presentacion=p,ubicacion__estructura__picking=True).order_by('fecha').first()


                    lineas = PedidoDetalle(transaccion=pedido, presentacion=p, cantidadcielo=rows[2],cantidad=num_unidades,
                                            costo=rows[3], importe=rows[4], subtotal=rows[5], impuestos=rows[6],
                                            total=rows[7], unidad=unidad,unidadcielo=und_cielo, ubicacion = pxu.ubicacion)
                    lineas.save()
                except Exception as e:
                    pedido.delete()
                    print(e)
                    cur.close()
                    con.close()
                    return (False, e)

            cur.close()
            con.close()
            return (True, '')


def almacenes(request):

    return render(request,'almacen/almcenes.html',context={'almacenes':Almacen.objects.all()})


def estructura(request):
    estructura = PresentacionxUbicacion.objects.filter(almacen_id=request.session['idalmacen']).order_by('ubicacion')

    return render(request,'almacen/estructura.html',context={'estructuras':estructura})

# def seleccionaHueco(request,pick=0):
#
#     if pick == 0:
#         picking = False
#     else:
#         picking = True
#
#     estruc = Estructura.objects.filter(almacen_id=request.session['idalmacen'],tipo__almacenaje=True,tipo__estructura__picking=picking)
#     return render(request,'almacen/estructura1.html',{'estructuras':estruc})


## Muestra el amacen para buscar una ubicacion
def asigna(request,idpresentacion,idtransa,idlinea):

    conf = Configuracion.objects.all().first()
    print(conf.entradasapickup)

    page = request.GET.get('page', 1)
    request.session['idpresentacion'] = idpresentacion

    descripcion = Presentacion.objects.get(codigo=idpresentacion)
    request.session['descripcion']=descripcion.descripcion

    request.session['idtransa'] = idtransa #En el caso de mover aquí se lleva el origen
    request.session['idlinea'] = idlinea

    lineaEntrada = EntradaDetalle.objects.get(pk=idlinea)


    mover = False

    # filtros = {'almacen_id':request.session['idalmacen'],'tipo__almacenaje':True, 'presentacionxubicacion__isnull':True}
    # if conf.entradasapickup == False:
    #     filtros['picking']=False

    if conf.entradasapickup: ## Entradas, se permite llevar a picking
        estructura = Estructura.objects.filter(almacen_id=request.session['idalmacen'], tipo__almacenaje=True,
                                                presentacionxubicacion__isnull=True).order_by('ubicacion')
        print(1)
    else:
        estructura =Estructura.objects.filter(almacen_id=request.session['idalmacen'], tipo__almacenaje=True,
                                              picking=False,presentacionxubicacion__isnull=True).order_by('ubicacion')
        print(2)


    #Estructura.objects.filter(**filtros)

    paginator = Paginator(estructura, 50)
    try:
        estructura = paginator.page(page)
    except PageNotAnInteger:
        estructura = paginator.page(1)
    except EmptyPage:
        estructura = paginator.page(paginator.num_pages)

    undsxpresenta = UnidadesxPresentacion.objects.get(presentacion=descripcion,unidadreferencia=descripcion.unidad,uso='E')

    if undsxpresenta:
        maximo = lineaEntrada.cantidadxasignar / undsxpresenta.unidades

        ## Buscar unidad de almancenamiento

        try:
            pale = UnidadesxPresentacion.objects.get(presentacion=descripcion,unidadreferencia=undsxpresenta.unidad,uso='A') ## Numero de unidades x pale
            if pale.unidades < maximo:
                maximo = pale.unidades
                paleund = pale.unidades
        except:
            maximo = 0
            paleund=0

    else:
        maximo = 0
        paleund = 0

    return render(request,'almacen/asignacion.html',context={'estructuras':estructura,'linea':lineaEntrada,'mover':mover,'undxpresenta':undsxpresenta,'maximo':maximo,'pale':paleund})


def MoverSeleccion(request,origen):

    conf = Configuracion.objects.all().first()

    page = request.GET.get('page', 1)

    prexubi = PresentacionxUbicacion.objects.get(pk=origen)

    descripcion = prexubi.presentacion.descripcion
    request.session['descripcion']=descripcion

    mover = True
    if conf.moverentreubicaciones: ##Mover, se permite mover de ubicaciones
        estructura = Estructura.objects.filter(almacen_id=request.session['idalmacen'], tipo__almacenaje=True,
                                                presentacionxubicacion__isnull=True).order_by('ubicacion')
    else:
        estructura =Estructura.objects.filter(almacen_id=request.session['idalmacen'], tipo__almacenaje=True,
                                              picking=True,presentacionxubicacion__isnull=True).order_by('ubicacion')


    paginator = Paginator(estructura, 50)
    try:
        estructura = paginator.page(page)
    except PageNotAnInteger:
        estructura = paginator.page(1)
    except EmptyPage:
        estructura = paginator.page(paginator.num_pages)

    #undsxpresenta = UnidadesxPresentacion.objects.get(presentacion=descripcion,uso='E')

    return render(request,'almacen/mover.html',context={'estructuras':estructura,'origen':origen})




## Guarda la ubicacion a una presentacion
def ubica(request,idubicacion,idlinea,cantidad,pale=0):

    idalmacen = request.session['idalmacen']
    lineaEntrada = EntradaDetalle.objects.get(pk=idlinea)
    idpresentacion = lineaEntrada.presentacion.codigo
    idtransa = lineaEntrada.transaccion.transaccion
    undxpre = lineaEntrada.presentacion.unidadesxpresentacion_set.filter(uso='E').first()
    unidad = undxpre.unidad
    unidades = undxpre.unidades
    print('Unidad {}'.format(unidad))



    print('Unidades {}'.format(unidades))
    print('Unidades por asigna {}'.format(lineaEntrada.cantidadxasignar))
    print('Unidades a restar {}'.format((cantidad*unidades)))
    if (cantidad*unidades) > lineaEntrada.cantidadxasignar:
        return render(request, 'almacen/error.html', {'error': 'La cantidad a asignar ({}) es mayor que la que queda por asignar ({})'.format((cantidad*unidades),lineaEntrada.cantidadxasignar), 'transa': idtransa})
    else:
        if pale == 1:
            cfg = Configuracion.objects.first()

            unds, created = UnidadesxPresentacion.objects.get_or_create(presentacion_id=idpresentacion,
                                                                    unidad_id=cfg.unidadalmacenaje,
                                                                    unidades=unidades,
                                                                    unidadreferencia=unidad, uso='A')

        pu = PresentacionxUbicacion(ubicacion_id=idubicacion, presentacion_id=idpresentacion, almacen_id=idalmacen,
                                    stock=cantidad, unidad=unidad, unidades=unidades, costo=lineaEntrada.costo)
        pu.save()

        lineaEntrada.cantidadxasignar = lineaEntrada.cantidadxasignar - (cantidad*unidades)
        lineaEntrada.save()


    request.session['idpresentacion'] = ''
    request.session['idlinea'] = ''
    request.session['descripcion'] = ''

    #calculo precio medio

    return redirect('Recupera',idtransa,1)

def PrecioMedio(detalle):

    #PreciosMedios.objects.get_or_create()
    pass



def asignacionAutomatica(request,idtransa):
    idalmacen = request.session['idalmacen']
    cabecera = EntradaCabecera.objects.get(id=idtransa)
    lineas = cabecera.detalle_set.filter(ubicacion__isnull=True)
    for linea in lineas:
        #Busco el hueco
        hueco = Estructura.objects.filter(almacen=request.session['idalmacen'] ,picking=False,tipo__almacenaje=True,presentacionxubicacion__isnull=True).first()

        if hueco:
            pu =PresentacionxUbicacion(ubicacion = hueco,presentacion=linea.presentacion,almacen_id=idalmacen,fecha=datetime.date.today())
            pu.save()

            linea.ubicacion = pu.ubicacion.ubicacion
            linea.save()
        else:
            return render(request,'almacen/error.html',{'error':'No hay huecos para hacer la asignación automática','transa':idtransa})


    return render(request,'almacen/entrada.html',context={'transaccion':cabecera,'ubicaciones':lineas})


def productos(request):
    idalmacen = request.session['idalmacen']
    presentaciones = PresentacionxUbicacion.objects.filter(almacen_id=idalmacen).order_by('presentacion','fecha')

    return render(request,'almacen/productos.html',context={'productos':presentaciones})

def moverproducto(request,idorigen,iddestino):
    origen = PresentacionxUbicacion.objects.get(pk=idorigen)

    destino=PresentacionxUbicacion(presentacion=origen.presentacion,ubicacion_id=iddestino,stock=origen.stock,costo=origen.costo,unidades=origen.unidades,
                           unidad=origen.unidad,almacen=origen.almacen)
    destino.save()
    origen.delete()


    return redirect('Productos')


class VaciaUbicacion(DetailView):
    model = PresentacionxUbicacion
    template_name = 'almacen/vaciaubicacion.html'
    context_object_name = 'presentacion'

def vacia(request,idubicacion):
    try:
        PresentacionxUbicacion.objects.get(id=idubicacion).delete()
    except:
        pass

    return redirect('Productos')


class DetalleUbicacion(DetailView):
    model = PresentacionxUbicacion
    template_name = 'almacen/deatalleubicacion.html'
    context_object_name = 'presentacion'


class pedidos(ListView):
    model = PedidoCabecera
    template_name = 'almacen/pedidos.html'
    context_object_name = 'pedidos'



class crearLinea(CreateView):
    template_name = 'producto.html'
    form_class = PedidoDetalleForm
    success_url = reverse_lazy('Pedido')




# def creaPedido(request):
#     template = 'almacen/pedido.html'
#     if request.method == 'GET':
#
#         form = PedidoForm(request.GET or None)
#         form_Set = PedidoDetalleFormSet(queryset=PedidoDetalle.objects.none())
#
#     elif request.method == 'POST':
#         form = PedidoForm(request.POST)
#         form_Set = PedidoDetalleFormSet(request.POST)
#
#         if form.is_valid() and form_Set.is_valid():
#             pedido=form.save()
#             print(pedido)
#
#
#             for f in form_Set:
#                 linea = f.save(commit = False)
#                 print(linea)
#                 linea.pedido=pedido
#                 print(linea.presentacion)
#                 print(linea.ubicacion)
#
#                 linea.save()
#
#             return redirect('Pedidos')
#
#
#     return render(request,template,{'form':form, 'formset':form_Set})


# def editaPedido(request,idpedido):
#     template = 'almacen/pedido_edit.html'
#     pedido = PedidoCabecera.objects.get(pk=idpedido)
#
#     if request.method == 'GET':
#
#         form = PedidoForm(instance=pedido)
#         form_Set = PedidoDetalleFormSet(instance=pedido)
#
#
#     elif request.method == 'POST':
#         form = PedidoForm(request.POST)
#         form_Set = PedidoDetalleFormSet(request.POST)
#
#         print(form_Set)
#
#         if form.is_valid() :
#             pedido=form.save()
#             print(pedido)
#             if form_Set.is_valid():
#
#                 for f in form_Set:
#                     linea = f.save(commit = False)
#                     print(linea)
#                     linea.pedido=pedido
#                     print(linea.presentacion)
#                     print(linea.ubicacion)
#
#                     linea.save()
#
#             return redirect('Pedidos')
#
#
#     return render(request,template,{'form':form, 'formset':form_Set})


def verPedido(request, idpedido):
    template = 'almacen/pedido_view.html'
    pedido = PedidoCabecera.objects.get(pk=idpedido)


    return render(request, template, {'pedido':pedido})

#@transaction.atomic
def procesarPedido(request,pk):
    ## Buscamos las ubicaciones y descontamos las cantidads
    pedido = PedidoCabecera.objects.get(pk=pk)

    if pedido.estado != 'P':
        return render(request, 'almacen/error.html',
                      {'error': 'Imposible procesar', 'texto': 'Pedido en estado {}'.format(pedido.estado)})


    for linea in pedido.pedidodetalle_set.all():

        try:

            ubi = PresentacionxUbicacion.objects.get(presentacion=linea.presentacion,ubicacion__ubicacion=linea.ubicacion)

            if linea.unidad.unidad == ubi.unidad:
                ubi.stock = ubi.stock - linea.cantidad
            else:

                ubi.stock = ((ubi.stock*ubi.unidades) - linea.cantidadcielo)/ubi.unidades

            ubi.save()
        except Exception as error:
 #           transaction.rollback()
            return render(request,'almacen/error.html',{'error':error,'texto': '{} - {}'.format(pedido.id,linea.presentacion)})

        pedido.estado = 'R'
        pedido.save()
  #      transaction.commit()
        return  render(request,'almacen/pedidos.html')


def cancelaPedido(request,pk):
    pedido = PedidoCabecera.objects.get(pk=pk)
    pedido.estado = 'C'
    pedido.save()




class creaPedido(CreateView):
    model = PedidoCabecera
    template_name = 'almacen/pedido.html'
    form_class = PedidoForm
    success_url = reverse_lazy('Pedidos')

    def get(self, request, *args, **kwargs):
        """Primero ponemos nuestro object como nulo, se debe tener en
        cuenta que object se usa en la clase CreateView para crear el objeto"""
        self.object = None
        # Instanciamos el formulario de Pedido que declaramos en la variable form_class
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        # Instanciamos el formset
        formset = PedidoDetalleFormSet()
        # Renderizamos el formulario del Pedido y el formset
        return self.render_to_response(self.get_context_data(form=form,formset=formset))

    def post(self, request, *args, **kwargs):
        # Obtenemos nuevamente la instancia del formulario de Pedido
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        # Obtenemos el formset pero ya con lo que se le pasa en el POST
        formset = PedidoDetalleFormSet(request.POST)
        """Llamamos a los métodos para validar el formulario de Compra y el formset, si son válidos ambos se llama al método
        form_valid o en caso contrario se llama al método form_invalid"""
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        # Aquí ya guardamos el object de acuerdo a los valores del formulario de Pedido
        self.object = form.save()
        # Utilizamos el atributo instance del formset para asignarle el valor del objeto Pedido creado y que nos indica el modelo Foráneo
        formset.instance = self.object
        # Finalmente guardamos el formset para que tome los valores que tiene
        formset.save()
        # Redireccionamos a la ventana del listado de compras
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        # Si es inválido el form de Compra o el formset renderizamos los errores
        return self.render_to_response(self.get_context_data(form=form,formset=formset))


class editaPedido(UpdateView):
    model = PedidoCabecera
    template_name = 'almacen/pedido_edit.html'
    form_class = PedidoForm
    success_url = reverse_lazy('Pedidos')

    def get(self, request, *args, **kwargs):
        """Primero ponemos nuestro object como nulo, se debe tener en
        cuenta que object se usa en la clase CreateView para crear el objeto"""
        self.object = self.get_object()
        # Instanciamos el formulario de Pedido que declaramos en la variable form_class
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        # Instanciamos el formset
        formset = PedidoDetalleFormSet(instance=self.object)
        # Renderizamos el formulario del Pedido y el formset
        return self.render_to_response(self.get_context_data(form=form,formset=formset))

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = PedidoDetalleFormSet(request.POST,instance=self.object)
        """Llamamos a los métodos para validar el formulario de Compra y el formset, si son válidos ambos se llama al método
        form_valid o en caso contrario se llama al método form_invalid"""
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        # Aquí ya guardamos el object de acuerdo a los valores del formulario de Pedido
        self.object = form.save()
        # Utilizamos el atributo instance del formset para asignarle el valor del objeto Pedido creado y que nos indica el modelo Foráneo
        formset.instance = self.object
        # Finalmente guardamos el formset para que tome los valores que tiene
        formset.save()
        # Redireccionamos a la ventana del listado de compras
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        # Si es inválido el form de Compra o el formset renderizamos los errores

        return self.render_to_response(self.get_context_data(form=form,formset=formset))


# # Para asignar la unidad en las entradas

def UnidadesxPresentacionPopUp(request,presentacionid,transaccionid,lineaid):

    presentacion = Presentacion.objects.get(pk=presentacionid)

    if request.method=='POST':
        form = UnidadxPresentacionForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data['unidad'])


            und = presentacion.unidadesxpresentacion_set.filter(unidad=form.cleaned_data['unidad'],uso='E')
            if und.exists() == False:
                #und,created = UnidadesxPresentacion.objects.get_or_create(presentacion=presentacion,unidad_id=form.cleaned_data['unidad'].pk,unidades=form.cleaned_data['unidades'],uso='E')
                pr = Presentacion.objects.get(codigo=presentacion.codigo)

                unds,created = UnidadesxPresentacion.objects.get_or_create(presentacion=presentacion, unidad=form.cleaned_data['unidad'],unidades=form.cleaned_data['unidades'], unidadreferencia=pr.unidad, uso='S')
                und,created = UnidadesxPresentacion.objects.get_or_create(presentacion=presentacion,unidad=form.cleaned_data['unidad'],unidades=form.cleaned_data['unidades'],unidadreferencia=pr.unidad,uso='E')


                ed = EntradaDetalle.objects.get(pk=lineaid)
                ed.unidad=und.unidad
                ed.save()

            #return reverse_lazy('Recupera',kwargs={'idtransa': transaccionid,'tipomov':1})
            return HttpResponseRedirect('/almacen/recupera/{}/1'.format(transaccionid))
    else:
        form = UnidadxPresentacionForm() #Aqui se puede poner la unidad por defecto de la configuracion
        return render(request,'almacen/addunidades.html',{'form':form,'presentacionid':presentacionid,'transaccionid':transaccionid,'lineaid':lineaid,'presentacion':presentacion})


#p= Presentacion.objects.filter(presentacionxubicacion__ubicacion__picking=True)

def undadesxpresentacion_query(request,presentacionid,uso,tipo):
    unds = UnidadesxPresentacion.objects.filter(presentacion_id=presentacionid,uso=uso).order_by('unidad')

    if tipo==1:
        template ='almacen/unidadesxpre_dropdown_options.html'
    elif tipo == 2:
        template = 'almacen/unidadesxpre_dropdown_2_options.html'

    return render(request, template, {'unds': unds})

def conversioncantidad(request,presentacionid,unidad1,unidad2,cantidad):
    unds = UnidadesxPresentacion.objects.filter(presentacion=presentacionid,unidad=unidad1,unidadreferencia_id=unidad2,uso='S' ).first()
    respuesta={}
    if unds:
        respuesta['cantidad'] = int(cantidad/unds.unidades)
    else:
        respuesta['cantidad'] = 1

    return HttpResponse(json.dumps(respuesta), content_type='application/json')



def ubicacionxpresentacion_query(request,presentacionid):

    prexub = PresentacionxUbicacion.objects.filter(presentacion_id=presentacionid, ubicacion__picking=True).order_by('fecha')

    respuesta={}
    if prexub.exists():
        respuesta['ubicacion'] = prexub.first().ubicacion.ubicacion
        respuesta['unidades'] = int(prexub.first().unidades)
        respuesta['unidad'] = prexub.first().unidad.codigo
        respuesta['stock']= int(prexub.first().stock)
    else:
        respuesta['ubicacion'] = 'No existe en Picking'


    return  HttpResponse(json.dumps(respuesta),content_type='application/json')
