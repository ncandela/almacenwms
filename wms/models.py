from django.db import models
from datetime import date

from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class TipoEstructura(models.Model):
    tipo = models.CharField(max_length=15,verbose_name='Tipo')
    estructurapadre = models.ForeignKey('TipoEstructura', on_delete=models.PROTECT, verbose_name='Estructura padre',null=True,blank=True)
    almacenaje = models.BooleanField(default=False,verbose_name='Permite almacenar')
    zonasalida = models.BooleanField(default=False,verbose_name='Zona de salida de pedidos')
    codificacion = models.CharField(max_length=5,default='')
    orden = models.PositiveIntegerField(default=10)

    def save(self, *args, **kwargs):
        if self.zonasalida and self.almacenaje:
            raise Exception('Puede ser zona de salida o permite almacenar, no los dos al mismo tiempo')
        super(TipoEstructura, self).save(*args, **kwargs)

    def __str__(self):
        return self.tipo


class Almacen(models.Model):
    nombre = models.CharField(max_length=25)
    direccion = models.CharField(max_length=150,null=True,blank=True)
    bloquearconfiguracionestructura = models.BooleanField(default=False)
    pordefecto = models.BooleanField(default=False)

    def save(self, *args, **kwargs):

        if self.pordefecto:
            Almacen.objects.all().exclude(pk=self.pk).update(pordefecto=False)

            super(Almacen,self).save( *args, **kwargs)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural='Almacenes'
        verbose_name = 'Almacen'


class EstructuraAlmacen(models.Model):
    almacen = models.ForeignKey(Almacen, on_delete=models.CASCADE)
    tipoestructura = models.ForeignKey('TipoEstructura', on_delete=models.PROTECT, verbose_name='Tipo estructura')
    alfabetico = models.BooleanField(default=False)
    autonumerar = models.BooleanField(default=False)

    caracteres = models.PositiveIntegerField(default=1)

    codigo = models.CharField(max_length=2,null=True,blank=True)
    cantidad = models.PositiveIntegerField(default=1)
    padre = models.ForeignKey('EstructuraAlmacen', on_delete=models.CASCADE, verbose_name='Estructura padre',null=True,blank=True)

    @property
    def tienehijos(self):
        return TipoEstructura.objects.filter(estructurapadre=self.tipoestructura).exists()

    def __str__(self):
        return '{} - {}'.format(self.tipoestructura,self.cantidad)

    class Meta:
        verbose_name_plural='Configuracion Estructuras Almacen'
        verbose_name = 'Configuracion Estructura Almacen'


class Estructura(models.Model):
    almacen = models.ForeignKey(Almacen,on_delete=models.CASCADE)
    ubicacion = models.CharField(max_length=15,db_index=True,unique=True)
    tipo = models.ForeignKey('TipoEstructura',on_delete=models.PROTECT)
    padre = models.ForeignKey('Estructura',on_delete=models.CASCADE,null=True,blank=True) #limit_choices_to={'tipo__almacenaje':False}
    picking = models.BooleanField(default=False,verbose_name='Picking')
    orden = models.PositiveIntegerField(default=0)
    presentacion = models.ForeignKey('Presentacion', on_delete=models.CASCADE, null=True, blank=True,verbose_name='Presentacion por defecto')

    def raiz(self):
        b = []
        e = self

        while e.padre != None:
            b.append(e.padre.ubicacion)
            e = e.padre

        b.reverse()

        return('/'.join(b))


    #def  save(self, *args, **kwargs):

    #    if self.tipo.almacenaje:
    #       d,created = PresentacionxUbicacion.objects.get_or_create()
    #
    #    super(Estructura, self).save(*args, **kwargs)

    def __str__(self):
        return self.ubicacion

    class Meta:
        verbose_name_plural='Estructuras'
        verbose_name = 'Estructura'


class Unidad(models.Model):
    codigo = models.CharField(max_length=10,primary_key=True,unique=True)
    descripcion = models.CharField(max_length=25)

    def __str__(self):
        return self.codigo

    class Meta:
        verbose_name_plural='Unidad'
        verbose_name = 'Unidades'
        ordering = ('codigo',)


class Grupo(models.Model):
    codigo = models.CharField(max_length=10,primary_key=True)
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = 'Grupos'
        verbose_name ='Grupo'



class Presentacion(models.Model):
    codigo = models.CharField(max_length=20,primary_key=True)
    descripcion = models.CharField(max_length=100)
    codarticulo = models.CharField(max_length=20, default='', null=True, blank=True)
    grupo = models.ForeignKey('Grupo',on_delete=models.PROTECT, null=True, blank=True)
    unidad = models.ForeignKey(Unidad,on_delete=models.CASCADE,verbose_name='Unidad Base')


    def __str__(self):
        return '{}-{}'.format(self.descripcion,self.codigo)

    class Meta:
        verbose_name='Producto'
        verbose_name_plural='Productos'
        ordering = ['descripcion','codigo']


@receiver(post_save, sender=Presentacion,dispatch_uid='comprueba_unidades')
def comprueba_unidades(sender, instance, **kwargs):
    print(instance)
    if instance.unidad and UnidadesxPresentacion.objects.filter(presentacion=instance,unidad=instance.unidad,uso='B').exists()==False:
        UnidadesxPresentacion(presentacion=instance,unidad = instance.unidad,unidades=1,unidadreferencia= instance.unidad,uso='B').save()


OPCIONESALMACENAMIENTO = (('E','Entrada'),('S','Salida'),('A','Almacenaje'),('B','Base'))

class UnidadesxPresentacion(models.Model):
    presentacion = models.ForeignKey(Presentacion, on_delete=models.CASCADE)
    unidad = models.ForeignKey(Unidad,on_delete=models.PROTECT,db_index=True,verbose_name='Unidad de medida') ## Los id son los nombres de las unidades
    unidades = models.DecimalField(max_digits=10,decimal_places=2,verbose_name='Cantidad en unidad de refencia')
    unidadreferencia = models.ForeignKey(Unidad,on_delete=models.PROTECT,default= '', related_name='unidadref' ,verbose_name='Referente a')
    uso = models.CharField(max_length=1,choices=OPCIONESALMACENAMIENTO)


    class Meta:
        unique_together = ("presentacion","unidadreferencia", "uso")
        verbose_name='Unidad x Presentacion'
        verbose_name_plural = 'Unidades x Presentacion'

    def save(self, *args, **kwargs):
        if self.unidad and not self.unidadreferencia:
            pr=Presentacion.objects.get(codigo=self.presentacion.codigo)
            self.unidadreferencia = pr.unidad
        super(UnidadesxPresentacion, self).save(*args, **kwargs)

    def __str__(self):
        return self.unidad.codigo



class PresentacionxUbicacion(models.Model):
    almacen = models.ForeignKey(Almacen, on_delete=models.PROTECT)
    ubicacion = models.ForeignKey(Estructura,on_delete=models.CASCADE)
    presentacion = models.ForeignKey(Presentacion,on_delete=models.CASCADE,null=True,blank=True)
    fecha = models.DateField(auto_now_add=True,null=True)
    unidad = models.ForeignKey(Unidad, on_delete=models.PROTECT,verbose_name='Unidade de medida')
    stock = models.DecimalField(max_digits=10, decimal_places=2, default=1)
    unidades = models.DecimalField(max_digits=12, decimal_places=4, default=1.0,verbose_name='Piezas en unidad de medida')
    costo = models.DecimalField(max_digits=15, decimal_places=4,default=0)

    def __str__(self):
        return '{}'.format(self.ubicacion)

    class Meta:
        verbose_name = 'Productos x Ubicacion'
        verbose_name_plural = 'Productos x Ubicacion'

class CabeceraPendienteManager(models.Manager):
    def pendientes(self):
        d = EntradaDetalle.objects.filter(transaccion=self, ubicacion__isnull=True).values(
            'transaccion_documento').distinct().exists()
        return d

class EntradaCabecera(models.Model):
    transaccion = models.CharField(max_length=15,db_index=True)
    fecha = models.DateField()
    tipo = models.CharField(max_length=10)
    proveedor = models.CharField(max_length=15)
    documento = models.CharField(max_length=15)
    almaorigen = models.CharField(max_length=15,verbose_name='Almacen Origen',null=True,blank=True)
    almadest = models.CharField(max_length=15,verbose_name='Almacen Destino',null=True,blank=True)
    moneda = models.CharField(max_length=3)
    importe = models.DecimalField(max_digits=15,decimal_places=2,default=0)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    impuestos = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    total = models.DecimalField(max_digits=15, decimal_places=2,default=0)

    asignada = models.BooleanField(default=False)
    fechaasignacion = models.DateTimeField(null=True,blank=True)


    def pendientes(self):
        d = EntradaDetalle.objects.filter(transaccion=self, ubicacion__isnull=True).values('transaccion_documento').distinct().exists()
        return d

    def __str__(self):
        return '{} - {} - {}'.format(self.transaccion,self.fecha,self.proveedor)


class EntradaDetalle(models.Model):
    transaccion = models.ForeignKey(EntradaCabecera,on_delete=models.CASCADE)
    linea = models.PositiveIntegerField()
    presentacion = models.ForeignKey(Presentacion,on_delete=models.PROTECT)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2,default=0)
    cantidadxasignar = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    costo = models.DecimalField(max_digits=15, decimal_places=4,default=0)
    importe = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    impuestos = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    total = models.DecimalField(max_digits=15, decimal_places=2,default=0)
    unidad = models.ForeignKey(Unidad,on_delete=models.DO_NOTHING,blank=True,null=True)
    ubicacion = models.CharField(max_length=15,null=True,blank=True)
    faltaunidad = models.BooleanField(default=False)



    def __str__(self):
        return '{} - {}'.format(self.presentacion.descripcion,self.cantidad)



class PreciosMedios(models.Model):
    presentacion = models.ForeignKey('Presentacion',on_delete=models.CASCADE)
    fecha = models.DateField(auto_now_add=True)
    pm = models.DecimalField(max_digits=12,decimal_places=4)
    stock = models.DecimalField(max_digits=12,decimal_places=2)

class Hotel(models.Model):
    nombre = models.CharField(max_length=25)

    def __str__(self):
        return self.nombre


OPCIONESMOV = ((0,'Salida'),(1,'Entrada'),(2,'Regularizacion'),(3,'Merma'))

class TipoMovimiento(models.Model):
    descripcion = models.CharField(max_length=15)
    tipomov = models.IntegerField(choices=OPCIONESMOV,default=1)

    def __str__(self):
        return self.descripcion

class TipoImpuesto(models.Model):
    descripcion = models.CharField(max_length=10)
    tipo = models.DecimalField(max_digits=5,decimal_places=2)

    def __str__(self):
        return '{} - {}'.format(self.descripcion,self.tipo)

ESTADOSPEDIDO = (('P','Pendiente'),
                 ('R','Procesado'),
                 ('C','Cancelado'))


class PedidoCabecera(models.Model):
    transaccion = models.CharField(max_length=15, db_index=True)
    fecha = models.DateField(auto_now_add=True)

    hotel = models.ForeignKey('Hotel',on_delete=models.PROTECT,null=True,blank=True)
    documento = models.CharField(max_length=15,null=True,blank=True)
    almadest = models.CharField(max_length=15, verbose_name='Almacen Destino', null=True, blank=True)
    moneda = models.CharField(max_length=3,blank=True,null=True)
    importe = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    impuestos = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    total = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    asignada = models.BooleanField(default=False)
    fechaasignacion = models.DateTimeField(null=True,blank=True)
    estado = models.CharField(max_length=1,default='P',choices=ESTADOSPEDIDO)

    def __str__(self):
        return '{} - {} - {}'.format(self.pk,self.transaccion,self.fecha)

class PedidoDetalle(models.Model):
    pedido = models.ForeignKey(PedidoCabecera,on_delete=models.CASCADE)
    presentacion = models.ForeignKey(Presentacion, on_delete=models.PROTECT)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2, default=1)
    cantidadcielo = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    costo = models.DecimalField(max_digits=15, decimal_places=4, default=0)
    importe = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    impuestos = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    total = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    unidadcielo = models.ForeignKey(Unidad,on_delete=models.PROTECT)
    unidad = models.ForeignKey(UnidadesxPresentacion, on_delete=models.DO_NOTHING, blank=True, null=True)
    ubicacion = models.CharField(max_length=15, null=True, blank=True)
    faltamaterial = models.BooleanField(default=False)

    def __str__(self):
        return '{} - {} - {}'.format(self.presentacion,self.cantidadcielo,self.ubicacion)

class Configuracion(models.Model):
    entradasapickup = models.BooleanField(default=False,verbose_name='Permitir entradas directas a zonas de PickUp')
    moverentreubicaciones = models.BooleanField(default=False,verbose_name='Permitir mover entre ubicaciones')
    moventrada = models.CharField(max_length=15,default='INTERDEP',blank=True,verbose_name='Movimientos a consultar para las entradas')
    movsalida  = models.CharField(max_length=15,default='ORDENCOMPRA',blank=True,verbose_name='Movimientos a consultar para las salidas')
    unidadpordefecto = models.ForeignKey(Unidad, on_delete=models.PROTECT, blank=True, null=True,verbose_name='Unidad por defecto')
    unidadalmacenaje = models.ForeignKey(Unidad, on_delete=models.PROTECT, related_name='undalma', blank=True, null=True,verbose_name='Unidad almacenaje por defecto')

    def __str__(self):
        return 'Configuracion'