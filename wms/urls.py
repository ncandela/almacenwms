
from django.contrib import admin
from django.urls import path,include
from wms.views import carga,index,recuperamov,asigna,ubica,productos,asignacionAutomatica,moverproducto,pedidos,creaPedido,\
    VaciaUbicacion,DetalleUbicacion,vacia,estructura,Desglosa,\
    UnidadesxPresentacionPopUp,MoverSeleccion,\
    undadesxpresentacion_query,ubicacionxpresentacion_query,conversioncantidad,verPedido,editaPedido,cancelaPedido,procesarPedido
#,Movimientos,CrearMovimiento,MovimientoDetail,
#PresentacionAutocomplete,UnidadAutocomplete,UbicacionAutocomplete,
#PresentacionxUbicacionAutocomplete,
admin.site.site_header = "WMS Oasis"
admin.site.site_title = "WMS Admin Portal"
admin.site.index_title = "Bienvenido a WMS Oasis"


urlpatterns = [

    path('',estructura,name='Index'),
    path('index/',estructura,name='Index'),
    path('carga/',carga,name='Carga'),
    path('entradas/',carga,name='Entrada'),
    path('salidas/',carga,name='Salida'),
    path('recupera/<idtransa>/<int:tipomov>/',recuperamov,name='Recupera'),

    path('estructura/',estructura,name='Estructura'),
    path('asigna/<idpresentacion>/<idtransa>/<idlinea>/',asigna,name='Asigna'),
    path('mover/<origen>/',MoverSeleccion,name='MoverSel'),

    path('asignaauto/<idtransa>/',asignacionAutomatica,name='AsignaAuto'),
    path('ubica/<idubicacion>/<idlinea>/<int:cantidad>/',ubica,name='Ubica'),
    path('ubican/<idubicacion>/<idlinea>/<int:cantidad>/<int:pale>/',ubica,name='Ubican'),
    path('productos/',productos,name='Productos'),
    path('mover/<idorigen>/<iddestino>/',moverproducto,name='Mover'),
    path('pedidos/',pedidos.as_view(),name='Pedidos'),
    path('pedido/',creaPedido.as_view(),name = 'Pedido'),
    path('verpedido/<int:idpedido>/',verPedido,name = 'VerPedido'),
    path('editapedido/<int:pk>/',editaPedido.as_view(),name = 'EditarPedido'),
    path('procesapedido/<int:pk>/', procesarPedido, name='ProcesaPedido'),
    path('cancelapedido/<int:pk>/', cancelaPedido, name='CancelaPedido'),

    # path('presentacion-autocomplete/',PresentacionAutocomplete.as_view(),name='presentacion-autocomplete'),
    # path('unidades-autocomplete/',UnidadAutocomplete.as_view(),name='unidad-autocomplete'),
    # path('estructura-autocomplete/',UbicacionAutocomplete.as_view(),name='ubicacion-autocomplete'),
    # path('prexubi-autocomplete/',PresentacionxUbicacionAutocomplete.as_view(),name='presentxubicacion-autocomplete'),
    path('vaciar/<int:pk>/',VaciaUbicacion.as_view(),name = 'VaciarUbicacion'),
    path('vacia/<int:pk>/',vacia,name = 'VaciaUbicacion'),
    path('detalleubicacion/<int:pk>/',DetalleUbicacion.as_view(),name = 'DetalleUbicacion'),

    # path('movimientos/',Movimientos.as_view(),name = 'Movimientos'),
    # path('creamovimiento/<int:tipo>/',CrearMovimiento,name = 'CreaMovimiento'),
    # path('creamovimiento/<int:tipo>/<int:pk>/',CrearMovimiento,name = 'AddMovimiento'),
    # path('detallemovimiento/<int:pk>/',MovimientoDetail.as_view(),name = 'DetalleMovimiento'),

    path('desglose/<int:pk>/',Desglosa,name = 'Desglose'),

    path('addundspopup/<presentacionid>/<transaccionid>/<lineaid>/',UnidadesxPresentacionPopUp,name='AddUnidades'),
    path('queryundsxpre/<presentacionid>/<uso>/<int:tipo>/',undadesxpresentacion_query,name='unidadxpre-autocomplete'),
    path('queryubicacion/<presentacionid>/',ubicacionxpresentacion_query,name='presentxubicacion'),
    path('conversion/<presentacionid>/<unidad1>/<unidad2>/<int:cantidad>/',conversioncantidad,name='conversioncantidad'),

]
