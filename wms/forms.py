from django.forms import ModelForm,ValidationError
from django.forms.models import inlineformset_factory,formset_factory
from .models import EntradaCabecera,EntradaDetalle,PedidoCabecera,PedidoDetalle,Presentacion,UnidadesxPresentacion,Estructura,PresentacionxUbicacion #MovimientoCab,MovimientoLin,
# from dal import autocomplete
# from dal import forward


from django import forms



class UnidadxPresentacionForm(forms.ModelForm):

    class Meta:
        model = UnidadesxPresentacion
        fields = ['unidad','unidades']



# class MovimientoForm(forms.ModelForm):
#
#     class Meta:
#         model = MovimientoCab
#         fields = ['tipo','fecha',]
#
#     def __init__(self, *args, **kwargs):
#         super(MovimientoForm, self).__init__(*args, **kwargs)
#
#         self.fields['fecha'].widget.input_type = 'date'
#
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })
#
#
#     def __init__(self, *args, **kwargs):
#         super(MovimientoForm, self).__init__(*args, **kwargs)
#
#         self.fields['fecha'].widget.input_type = 'date'
#
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })

# class MovimientoLinForm(forms.ModelForm):
#     presentacion = forms.ModelChoiceField(
#          queryset=Presentacion.objects.all(),
#          widget= autocomplete.ModelSelect2(url='presentacion-autocomplete')
#      )
#     unidad = forms.ModelChoiceField(
#         queryset=UnidadesxPresentacion.objects.all(),
#         widget= autocomplete.ModelSelect2(url='unidad-autocomplete',forward=(forward.Field('presentacion'),))
#     )
#
#     ubicacion = forms.ModelChoiceField(
#         queryset=Estructura.objects.all(),
#         widget=autocomplete.ModelSelect2(url='ubicacion-autocomplete')
#     )
#
#     class Meta:
#         model = MovimientoLin
#         fields = ['presentacion','cantidad','unidad','ubicacion']
#         #widgets = {'presentacion': autocomplete.ModelSelect2(url='presentacion-autocomplete')}
#
#     def __init__(self, *args, **kwargs):
#         super(MovimientoLinForm, self).__init__(*args, **kwargs)
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })
#
#     def clean_cantidad(self):
#         cantidad = self.cleaned_data['cantidad']
#         if cantidad <= 0:
#             raise forms.ValidationError("Debe ingresar una cantidad valida")
#         return cantidad
#
#     # def clean_importe(self):
#     #     precio = self.cleaned_data['importe']
#     #     if precio <= 0:
#     #         raise forms.ValidationError("Debe ingresar un precio valido")
#     #     return precio
#
#     def clean_unidad(self):
#         und = self.cleaned_data['unidad']
#         presenta = self.cleaned_data['presentacion']
#         if UnidadesxPresentacion.objects.filter(presentacion=presenta,unidad=und).exists() == False:
#             raise forms.ValidationError("No existe esa unidad para esa presentacion")
#         return self.unidad
#
#
#
# MovimientoLinFormSet = inlineformset_factory(MovimientoCab, MovimientoLin, form=MovimientoLinForm, extra=1)




# class PedidoDetalleForm(forms.ModelForm):
#     presentacion = forms.ModelChoiceField(
#          queryset=PresentacionxUbicacion.objects.filter(ubicacion__estructura__picking=True),
#          widget= autocomplete.ModelSelect2(url='presentacion-autocomplete')
#      )
#
#
#     unidad = forms.ModelChoiceField(
#         queryset=UnidadesxPresentacion.objects.all(),
#         widget= autocomplete.ModelSelect2(url='presentxubicacion-autocomplete',forward=(forward.Field('presentacion'),))
#     )
#
#     ubicacion = forms.ModelChoiceField(
#         queryset=Estructura.objects.all(),
#         widget=autocomplete.ModelSelect2(url='ubicacion-autocomplete')
#     )
#
#     class Meta:
#         model = MovimientoLin
#         fields = ['presentacion','cantidad','unidad','ubicacion']
#         #widgets = {'presentacion': autocomplete.ModelSelect2(url='presentacion-autocomplete')}
#
#     def __init__(self, *args, **kwargs):
#         super(MovimientoLinForm, self).__init__(*args, **kwargs)
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })
#
#     def clean_cantidad(self):
#         cantidad = self.cleaned_data['cantidad']
#         if cantidad <= 0:
#             raise forms.ValidationError("Debe ingresar una cantidad valida")
#         return cantidad
#
#     # def clean_importe(self):
#     #     precio = self.cleaned_data['importe']
#     #     if precio <= 0:
#     #         raise forms.ValidationError("Debe ingresar un precio valido")
#     #     return precio
#
#     def clean_unidad(self):
#         und = self.cleaned_data['unidad']
#         presenta = self.cleaned_data['presentacion']
#         if UnidadesxPresentacion.objects.filter(presentacion=presenta,unidad=und).exists() == False:
#             raise forms.ValidationError("No existe esa unidad para esa presentacion")
#         return self.unidad
#
#
#
#




class EntradaDetalleForm(ModelForm):
    class Meta:
        model = EntradaDetalle
        fields = ('presentacion','cantidad','unidad')


class Ubucacion(ModelForm):
    class Meta:
        model = EntradaDetalle
        fields = ('ubicacion',)


class Unidad(ModelForm):
    class Meta:
        model = EntradaDetalle
        fields = ('unidad',)

class PedidoForm(ModelForm):

    class Meta:
        model = PedidoCabecera
        fields = ['hotel','transaccion','documento','almadest']

    def __init__(self, *args, **kwargs):
        super(PedidoForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):

            self.fields[field].widget.attrs.update({
                'class': 'form-control  form-control-sm'
            })

class PedidoDetalleForm(ModelForm):

    presentacion = forms.ModelChoiceField(queryset= Presentacion.objects.filter(presentacionxubicacion__ubicacion__picking=True).distinct())

    class Meta:
        model = PedidoDetalle
        fields = ['presentacion','cantidadcielo','unidadcielo','cantidad','unidad','ubicacion']

    def __init__(self, *args, **kwargs):
        super(PedidoDetalleForm, self).__init__(*args, **kwargs)
        # for field in iter(self.fields):
        #     self.fields[field].widget.attrs.update({
        #         'class': 'form-control form-control-sm '
        #     })
        self.fields['presentacion'].widget.attrs.update({'class': 'form-control form-control-sm presentacion'})
        self.fields['cantidadcielo'].widget.attrs.update({'class': 'form-control form-control-sm cantidadcielo'})
        self.fields['unidadcielo'].widget.attrs.update({'class': 'form-control form-control-sm unidadcielo'})
        self.fields['cantidad'].widget.attrs.update({'class': 'form-control form-control-sm cantidad'})
        self.fields['unidad'].widget.attrs.update({'class': 'form-control form-control-sm unidad'})
        self.fields['ubicacion'].widget.attrs.update({'class': 'form-control form-control-sm ubicacion'})
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['ubicacion'].widget.attrs['readonly'] = True

        #self.fields['unidad'].queryset = UnidadesxPresentacion.objects.none()



    # def clean_cantidad(self):
    #     cantidad = self.cleaned_data['cantidad']
    #     if cantidad == '':
    #         raise ValidationError("Debe ingresar una cantidad valida")
    #     return cantidad

PedidoDetalleFormSet = inlineformset_factory(PedidoCabecera,PedidoDetalle,form=PedidoDetalleForm,extra=1)