from django import template
from django.template.defaultfilters import stringfilter,register


@register.simple_tag()
def multiplicar(a, b, c=1, *args, **kwargs):

    m = a * b * c
    #
    # for arg in args:
    #     m= m * args

    return round(m,2)

