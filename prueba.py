import cx_Oracle
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "almacenwms.settings")
django.setup()
from wms.models import Presentacion,Unidad,Grupo

try:
    con = cx_Oracle.connect('algoc15/service@192.168.1.25:1521/ZEUS')

except Exception as e:
    print('Error: {}'.format(e))


cur = con.cursor()

sql_gr = '''select GR_GRUPO,GR_DESC  from algrupos'''
try:
    cur.execute(sql_gr)
    print('')
except Exception as e:
    print(e)
    cur.close()
    con.close()

rows = cur.fetchall()

for row in rows:
    grp,created = Grupo.objects.get_or_create(codigo=row[0],descripcion=row[1])

Presentacion.objects.all().delete()

sql = '''SELECT A.PN_PRESENTA, A.PN_ARTICULO,A.PN_DESC,A.PN_UNIDAD_ENTRADA,P.P_GRUPO FROM ALPRESENTA A LEFT JOIN ALPRODUCTOS P ON PN_ARTICULO = P.P_ARTICULO 
WHERE A.PN_PRESENTA IN ('100013','100048','100075','100081','100104','109685','230491','230491','230492','230495','230622','104179','186760','229788','247399','247400','247401')'''

##P.P_GRUPO IN ({grupo}) AND P.P_ART_ACTIVO='S' '''.format(grupo='10,22,29,16,18,59,60,61')

try:
    cur.execute(sql)
    print('')
except Exception as e:
    print(e)
    cur.close()
    con.close()

rows = cur.fetchall()

for row in rows:

    unidad = row[3]

    if row[3] in ('PIEZS',
                    'PIEAZA',
                    'PIEZAS.',
                    'PIEIZA',
                    'PIEZAS',
                    'PIUEZAS',
                    'PIZA',
                    'PIZAS',
                    'PIEZ<AS',
                    'PIPEZA',
                    'PIEZAAS',
                    'PIEAS',
                    'PIEZAS1',
                    'PIIEZA',
                    'PIEA',
                    'PIEZSA',
                    'PIPEZAS',
                    'PIUEZA',
                    'PZA',
                  'PZAS',
                  ' PIEZAS',
                  'POEZA',
                  'PUEZA',
                  'UNIDAD',
                  'PEZAS',
                  'PEIZA',
                  '1'):
        unidad='PIEZA'

        if row[3] in ('KILOS','KLO',' KILOS','KILOA','KILOGRAMOS'):
            unidad = 'KILO'

        if row[3] in ('MTS','METROS',' METROS','MATRO'):
            unidad = 'METRO'

        if row[3] in ('MTS','METROS','MATRO','TRAMO','ROLLO'):
            unidad = 'METRO'

        if row[3] in ('PAQUETE','CAJAS'):
            unidad = 'CAJA'

        if row[3] in ('LOTRO', 'LOTROS','LITROS'):
            unidad = 'LITRO'


        if row[3] in ('GARRAFON'):
            unidad = 'GARRAFA'

        if row[3] in ('CUEBTA', 'CUBTE','CUBEYA','CUBTE'):
            unidad = 'CUEBETA'

        if row[3] in ('BOLSAS','BULTO'):
            unidad = 'BOLSA'

        if row[3] in ('BOTE'):
            unidad = 'LATA'

    print(row)

    und,created = Unidad.objects.get_or_create(codigo=unidad,descripcion=unidad)

    prs,created = Presentacion.objects.get_or_create(codigo=row[0],descripcion=row[2],codarticulo=row[1],grupo_id=row[4], unidad=und)