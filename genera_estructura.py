
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "almacenwms.settings")
django.setup()

from wms.views import creaestructura
from wms.models import Almacen

alma = Almacen.objects.get(pordefecto=True)
creaestructura(idAlmacen=alma.pk)