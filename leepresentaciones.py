import cx_Oracle
import os
import django

import csv


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "almacenwms.settings")
django.setup()
from wms.models import Presentacion,Unidad,Grupo

try:
    con = cx_Oracle.connect('algoc15/service@192.168.1.25:1521/ZEUS')

except Exception as e:
    print('Error: {}'.format(e))
    exit(1)

cur = con.cursor()

#Presentacion.objects.all().delete()



file = open('PRODUCTOSCHINA.csv')
lineas = csv.reader(file, delimiter=',')
for linea in lineas:
    print('_'*80)
    sql = '''SELECT PN_PRESENTA, PN_ARTICULO,PN_DESC,PN_UNIDAD_ENTRADA FROM ALPRESENTA WHERE PN_PRESENTA = '{}' '''.format(linea[0])

    try:
        #cur.prepare(sql)


        cur.execute(sql)

    except Exception as e:
        print(e)
        cur.close()
        con.close()
        exit(1)

    row = cur.fetchone()
    print(row)

    unidad = row[3]

    if row[3] in ('PIEZS',
                    'PIEAZA',
                    'PIEZAS.',
                    'PIEIZA',
                    'PIEZAS',
                    'PIUEZAS',
                    'PIZA',
                    'PIZAS',
                    'PIEZ<AS',
                    'PIPEZA',
                    'PIEZAAS',
                    'PIEAS',
                    'PIEZAS1',
                    'PIIEZA',
                    'PIEA',
                    'PIEZSA',
                    'PIPEZAS',
                    'PIUEZA',
                    'PZA',
                  'PZAS',
                  ' PIEZAS',
                  'POEZA',
                  'PUEZA',
                  'UNIDAD',
                  'PEZAS',
                  'PEIZA',
                  '1'):
        unidad='PIEZA'

    if row[3] in ('KILOS','KLO',' KILOS','KILOA','KILOGRAMOS'):
        unidad = 'KILO'

    if row[3] in ('MTS','METROS',' METROS','MATRO'):
        unidad = 'METRO'

    if row[3] in ('MTS','METROS','MATRO','TRAMO','ROLLO'):
        unidad = 'METRO'

    if row[3] in ('PAQUETE','CAJAS'):
        unidad = 'CAJA'

    if row[3] in ('LOTRO', 'LOTROS','LITROS'):
        unidad = 'LITRO'


    if row[3] in ('GARRAFON'):
        unidad = 'GARRAFA'

    if row[3] in ('CUEBTA', 'CUBTE','CUBEYA','CUBTE'):
        unidad = 'CUEBETA'

    if row[3] in ('BOLSAS','BULTO'):
        unidad = 'BOLSA'

    if row[3] in ('BOTE'):
        unidad = 'LATA'



    if  Unidad.objects.filter(codigo=unidad).exists() == False:
        und = Unidad(codigo=unidad,descripcion=unidad).save()
    else:
        und = Unidad.objects.get(codigo=unidad)

    grupo = Grupo.objects.get(codigo=linea[1])

    pr =Presentacion.objects.filter(codigo=row[0]).first()

    if not pr:
        pr=Presentacion(codigo=row[0],descripcion=row[2],codarticulo=row[1],grupo=grupo, unidad=und)
        pr.save()
        print('Save')

    print(pr.codigo)
    pr2 = Presentacion.objects.get(codigo=pr.codigo)
    print(pr2)

cur.close()
con.close()