import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "almacenwms.settings")
django.setup()
from wms.models import Presentacion,Unidad,Grupo,EntradaDetalle

detalles = EntradaDetalle.objects.all()
for detalle in detalles:
    und = detalle.presentacion.unidadesxpresentacion_set.filter(uso='E')
    if und:
        unidad = und[0].unidad
    else:
        unidad = None

    detalle.unidad = unidad
    detalle.cantidadxasignar = detalle.cantidad
    detalle.save()


